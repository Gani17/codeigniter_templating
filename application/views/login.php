<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<?php $this->load->view('layouts/headers') ?>
<?php $this->load->view('layouts/navbar') ?>
<?php $this->load->view('layouts/content') ?>
<?php $this->load->view('layouts/footers') ?>
    <div class="Bg">
        <div class="box">
              <h2>Login</h2>
            <form action="<?=site_url('indexAdmin') ?>" method="post">
              <div class="box1">
                <input type="text" name="" value="" required><br>
                <label>Username</label><br><br>
              </div>
              <div class="box2">
                <input type="password" name="" value="" required>
                <label>Password</label>
              </div>
              <a href="<?=site_url('Register')?>" class="reg">Register</a>
              <a href="#"class="forget">Forget Password</a>
              <a href="<?=site_url('Welcome') ?>"class="close">&#10006;</a>
              <button type="submit" name="button">Login</button>
            </form>
        </div>
    </div>
  </body>
</html>
