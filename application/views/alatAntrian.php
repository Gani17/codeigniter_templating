<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Detail</title>
    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
  </head>
  <body>
    <div class="navBar navDetail">
      <ul>
        <li><a href="<?=site_url('Welcome')?>">Home</a></li>
        <li><a href="<?=site_url('Welcome')?>#Profil">Profile</a></li>
        <li><a href="<?=site_url('Welcome')?>#Product">Product</a></li>
        <li><a href="<?=site_url('Welcome')?>#Contact">Contact</a></li>
        <li><a href="<?=site_url('Login')?>">Login</a></li>
        <li><a href="<?=site_url('Register')?>">Register</a></li>
      </ul>
    </div>
    <div class="details">
      <h1>Alat Kepuasan Pelanggan</h1>
      <img src="http://www.alatantrian.com/wp-content/uploads/2018/04/Alat-Survey-Kepuasan-Pelanggan-e1524724742127.jpg" alt="">
      <p>Alat SKP merupakan Produk Software untuk pelayanan umum seperti : Bank, Rumah Sakit, Pemesanan Tiket Kereta Api, Pesawat Terbang, PLN, PDAM, Kantor Pos dan berbagai pelayanan publik lainnya yang memerlukan software ini sebagai alat pengaturan antrian. Fitur-fitur yang disediakan dalam program ini pun kami usahakan untuk dapat memenuhi berbagai macam kebutuhan yang diperlukan saat implementasi sistem antrian oleh pengguna sehingga banyak varian yang muncul dari setiap tahap pengembangan yang ada disebabkan karena antara satu unit/entitas pengguna cenderung ada perbedaan dengan entitas pengguna lainnya. Harapan kami semoga dengan kehadiran software ini, dapat berkontribusi dalam membantu perusahaan-perusahaan pelayanan publik di Indonesia dalam hal  dapat memberikan kepuasan serta kenyamanan bagi para pelanggan yang berada dalam antrian. Software  diberi sentuhan tampilan baru. Tampilan kepuasan pada layar LCD TV mengalami perubahan agar semakin sesuai dengan kebutuhan pelanggan yang terus berkembang.
      </p>
      <div class="fitur">
        <ul>
          <li><h2>Fitur-fitur</h2></li>
          <li>Komputer</li>
          <li>Speaker aktif</li>
          <li>Monitor touchscreen</li>
          <li>LED TV 43"</li>
          <li>Printer termal</li>
          <li>kabel USB HUB</li>
          <li>VGA double view</li>
        </ul>
      </div>
    </div>
      <footer>
        <div class="wrapper footer">
          <img src="<?php echo base_url('assets/img/sea.svg') ?>">
        </div>
        <div class="sosmed">
            <img src="https://www.harperwoodslibrary.org/site-assets/images/fb.png/@@images/image.png" width="50px" alt="fb">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/2000px-Instagram_logo_2016.svg.png" width="50px;" alt="instagram">
            <img src="https://pngimg.com/uploads/twitter/twitter_PNG32.png" width="50px" alt="twtter">
            <img src="https://3.bp.blogspot.com/-jJ1y-T1qPxo/V15WQgXfRHI/AAAAAAAAALw/Q3AwAi1_PYwUwq_qeT42A0mzqVY_iUWjACKgB/s640/unnamed.png" width="50px" alt="Path">
            <img src="https://4.bp.blogspot.com/-DRiiS6am79E/WIqxTRTn1jI/AAAAAAAACUk/0ChtERF3934cSOHLwKY5ko7-AACTb7YbgCLcB/s400/VEKTOR%2BICON2.png" width="50px" alt="Whatsapp">
            <br><br>
          <a href="#">Privacy</a> | <a href="#">Terms of use</a> | <a href="#">Cookies Policy</a>
          <br><br>
          ©2018 PT.Cendana Teknika Utama  All Right Reserved
        </div>
      </footer>
  </body>
</html>
