<div class="header">
  <h1>CENDANA2000</h1>
  <div class="wrapper">
    <img src="<?php echo base_url('assets/img/meeting.svg ') ?>" alt="team" >
    <h3>Team Profesional</h3>
    <p>Kami memiliki para ahli yang berkompeten  pada bidangnya dan selalu menjaga privacy client bersangkutan</p>
  </div>
  <div class="wrapper two">
    <img src="<?php echo base_url('assets/img/achievement.svg') ?>">
    <h3>Best Product</h3>
    <p>Memberikan solusi yang tepat dan senantiasa memberikan respon pelayanan yang yang cepat kepada pelanggan</p>
  </div>
  <div class="wrapper three">
    <img src="<?php echo base_url('assets/img/performance.svg') ?>">
    <h3>Fast Perfomance</h3>
    <p>Product yang kami buat sangat baik untuk kebutuhan anda dengan
      performance tercepat dalam pengerjaan cepat</p>
  </div>
</div>
<div class="Profile"id="Profil">
  <h1>Profile</h1>
  <hr>
    <div class="images">
      <img src="<?php echo base_url('assets/img/1.png') ?>">
    </div>
    <div class="img">
      <img src="<?php echo base_url('assets/img/2.png') ?>">
    </div>
  <div class="wrapper">
    <p>Divisi IT Multimedia merupakan divisi atau bidang usaha kami yang
       fokus di bidang multimedia, yaitu berbagai alat yang digunakan
       sebagai media penyampaian informasi yang di dalamnya terdapat
       perpaduan (kombinasi) berbagai bentuk elemen informasi, seperti teks,
       graphics, animasi, video, interaktif maupun suara. Dengan tenaga-tenaga
       professional yang ada, divisi kami memproduksi dan menjual alat-alat
       tersebut yang dikembangkan sesuai dengan perkembangan teknologi terkini.</p><br>
    <p>Kami adalah perusahaan yang bergerak di bidang teknologi informasi
      dan telekomunikasi.Seiring dengan perkembangan jaman diiringi dengan
      perubahan dan kemajuan.
      teknologi informasi dan telekomunikasi yang begitu cepat, hingga
      tahun 2016 ini kami memiliki 5 divisi/bidang usaha yang kami sesuaikan</p>
    <p>dengan perkembangan dan kebutuhan terkini. PT. Cendana Teknika Utama,
      PT. Cendana Teknika Utama, mempunyai kemampuan managerial, keahlian dan
      keuangan yang baik, sehingga mampu menjadi perusahaan yang terus
      berkembang untuk menghasilkan produk dan jasa layanan terbaik bagi penggunanya.</p>
  </div>
</div>
<div class="Product"id="Product">
  <h1>Product</h1>
  <hr>
  <ul>
    <a href="<?=site_url('Antrian')?>">
      <li>
      <h3>Alat Antrian</h3>
      <img src="https://4.bp.blogspot.com/-teZerwk9B2w/VpBvedAQgDI/AAAAAAAAADI/yrsWRs-1WxU/s400/CYG-vuvUAAAS5g6.jpg" width="250">
      <span>
        <h2>Alat Antrian</h2><br>
          <p>alat untuk menampilkan nomer antrian
             mesin antrian android, mesin antrian touchscreen, mesin antrian
              microq, mesin antrian led running text, dan mesin antrian minimalis</p><br><br>
          <form action="<?=site_url('Antrian')?>" method="get">
          <button type="submit" name="button">Lihat Detail</button>
          </form>
              </span>
            </li>
          </a>
          <a href="<?=site_url('Antrian')?>">
          <li>
            <h3>Q Mobile</h3>
            <img src="http://www.alatantrian.com/wp-content/uploads/2018/07/Mesin-Antrian-Q-Mobile-1024x768.jpg" width="300"height="270">
            <span>
              <h2>Q Mobile</h2><br>
              <p>alat untuk menampilkan nomer antrian
                mesin antrian android, mesin antrian touchscreen, mesin antrian
                microq, mesin antrian led running text, dan mesin antrian minimalis</p><br><br>
                <form action="<?=site_url('Antrian')?>" method="get">
                  <button type="submit" name="button">Lihat Detail</button>
                </form>
            </span>
          </li>
        </a>
        <a href="<?=site_url('Antrian')?>">
        <li>
          <h3>Antrian Android</h3>
          <img src="http://www.alatantrian.com/wp-content/uploads/2018/07/mesin-antrian-android-1024x768.jpg" width="300"height="270">
          <span>
            <h2>Antrian Android</h2><br>
            <p>alat untuk menampilkan nomer antrian
              mesin antrian android, mesin antrian touchscreen, mesin antrian
              microq, mesin antrian led running text, dan mesin antrian minimalis</p><br><br>
              <form action="<?=site_url('Antrian')?>" method="get">
                <button type="submit" name="button">Lihat Detail</button>
              </form>
          </span>
        </li>
      </a>
      <a href="<?=site_url('Antrian')?>">
      <li>
        <h3>Mesin Minimalis</h3>
        <img src="http://www.softwaremesinantrian.com/wp-content/uploads/2018/07/photo_2018-04-16_14-03-56.jpg" width="300"height="270">
        <span>
          <h2>Mesin Minimalis</h2><br>
          <p>alat untuk menampilkan nomer antrian
            mesin antrian android, mesin antrian touchscreen, mesin antrian
            microq, mesin antrian led running text, dan mesin antrian minimalis</p><br><br>
            <form action="<?=site_url('Antrian')?>" method="get">
              <button type="submit" name="button">Lihat Detail</button>
            </form>
        </span>
      </li>
    </a>
    <a href="<?=site_url('Antrian')?>">
    <li>
      <h3>Digital Signage</h3>
      <img src="http://www.alatantrian.com/wp-content/uploads/2018/04/Digital-Signage-e1524724724317.jpg" width="250">
      <span>
        <h2>Digital Signage</h2><br>
        <p>alat untuk menampilkan nomer antrian
          mesin antrian android, mesin antrian touchscreen, mesin antrian
          microq, mesin antrian led running text, dan mesin antrian minimalis</p><br><br>
          <form action="<?=site_url('Antrian')?>" method="get">
            <button type="submit" name="button">Lihat Detail</button>
          </form>
      </span>
    </li>
    </a>
    <a href="<?=site_url('Antrian')?>">
    <li>
      <h2>Anjungan Infomasi</h2>
      <img src="http://www.alatantrian.com/wp-content/uploads/2018/04/Anjungan-Informasi-e1524724703119.jpg" width="300">
      <span>
        <h2>Anjungan Informasi</h2><br>
        <p>alat untuk menampilkan nomer antrian
          mesin antrian android, mesin antrian touchscreen, mesin antrian
          microq, mesin antrian led running text, dan mesin antrian minimalis</p><br><br>
          <form action="<?=site_url('Antrian')?>" method="get">
            <button type="submit" name="button">Lihat Detail</button>
          </form>
      </span>
    </li>
    </a>
    <a href="<?=site_url('Antrian')?>">
    <li>
      <h3>Moving Sign</h3>
      <img src="http://www.alatantrian.com/wp-content/uploads/2018/04/Moving-Sign-e1524724684626.jpg" width="300">
      <span>
        <h2>Moving Sign</h2><br>
        <p>alat untuk menampilkan nomer antrian
          mesin antrian android, mesin antrian touchscreen, mesin antrian
          microq, mesin antrian led running text, dan mesin antrian minimalis</p><br><br>
          <form action="<?=site_url('Antrian')?>" method="get">
            <button type="submit" name="button">Lihat Detail</button>
          </form>
      </span>
    </li>
    </a>
    <a href="<?=site_url('Antrian')?>">
    <li>
      <h3>Video Tron</h3>
      <img src="http://www.alatantrian.com/wp-content/uploads/2018/04/Video-Tron-e1524724693816.jpg" width="300">
      <span>
        <h2>Video Tron</h2><br>
        <p>alat untuk menampilkan nomer antrian
          mesin antrian android, mesin antrian touchscreen, mesin antrian
          microq, mesin antrian led running text, dan mesin antrian minimalis</p><br><br>
          <form action="<?=site_url('Antrian')?>" method="get">
            <button type="submit" name="button">Lihat Detail</button>
          </form>
      </span>
    </li>
    </a>
  </ul>
</div>
