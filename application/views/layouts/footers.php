<footer>
  <div class="wrapper">
    <img src="<?php echo base_url('assets/img/sea.svg') ?>">
  </div>
  <div class="social">
    <img src="https://www.harperwoodslibrary.org/site-assets/images/fb.png/@@images/image.png" width="50px" alt="fb">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/2000px-Instagram_logo_2016.svg.png" width="50px;" alt="instagram">
    <img src="https://pngimg.com/uploads/twitter/twitter_PNG32.png" width="50px" alt="twtter">
    <img src="https://3.bp.blogspot.com/-jJ1y-T1qPxo/V15WQgXfRHI/AAAAAAAAALw/Q3AwAi1_PYwUwq_qeT42A0mzqVY_iUWjACKgB/s640/unnamed.png" width="50px" alt="Path">
    <img src="https://4.bp.blogspot.com/-DRiiS6am79E/WIqxTRTn1jI/AAAAAAAACUk/0ChtERF3934cSOHLwKY5ko7-AACTb7YbgCLcB/s400/VEKTOR%2BICON2.png" width="50px" alt="Whatsapp">
    <br><br>
    <a href="#">Privacy</a> | <a href="#">Terms of use</a> | <a href="#">Cookies Policy</a>
    <br><br>
    ©2018 PT.Cendana Teknika Utama  All Right Reserved
  </div>
</footer>
</body>
</html>
