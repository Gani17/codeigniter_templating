<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Dasboard Admin</title>
  <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
  </head>
  <body>
    <div class="navBar Admin">
      <ul>
        <a href="<?=site_url('Welcome')?>"><img src="http://cendana2000.co.id/wp-content/uploads/2018/04/Cendana-2.png" alt=""></a>
        <li><a href="<?=site_url('Welcome')?>">Home</a></li>
        <li><a href="<?=site_url('Welcome')?>#Profil">Profile</a></li>
        <li><a href="<?=site_url('Welcome')?>Product">Product</a></li>
        <li><a href="#"><img src=""></a></li>
        <li><a href="<?=site_url('Welcome')?>">Admin</a></li>
      </ul>
    </div>
    <div class="navAdmin">
      <div class="img">
        <img src="<?php echo base_url('assets/img/nickYong.png') ?>" alt="nick"width="10">
        <h3>Nick Yong</h3>
        <span>Web Developer</span>
      </div>
      <a href="#">
      <div class="HomeAdmin home">
        <img src="<?php echo base_url('assets/img/home.svg') ?>" width="40">
        <h4>Dashboard</h4>
      </div>
      </a>
      <a href="#profile">
      <div class="HomeAdmin profile">
        <img src="<?php echo base_url('assets/img/user.svg') ?>" width="40">
        <h4>Profile</h4>
      </div>
      </a>
      <a href="#">
      <div class="HomeAdmin profile">
        <img src="<?php echo base_url('assets/img/email.svg') ?>" width="40">
          <h4>Massage</h4>
      </div>
      </a>
      <a href="#myProduct">
      <div class="HomeAdmin myproduct">
        <img src="<?php echo base_url('assets/img/app.svg') ?>" width="40">
        <h4>Product</h4>
      </div>
      </a>
      <a href="#">
      <div class="HomeAdmin page">
        <img src="<?php echo base_url('assets/img/page.svg') ?>" width="40">
        <h4>Page</h4>
      </div>
      </a>
    </div>
    <div class="dasboard">
        <div class="review">
          <ul>
            <li><img src="<?php echo base_url('assets/img/man.svg') ?>" alt="" width="100">
            </li><span>New Clients<h2>13</h2>
              <div class="outline small clients">
                <div class="inline smally client">
                </div>
              </div>
            </span>
            <li><img src="<?php echo base_url('assets/img/contract.svg') ?>" alt="" width="100">
            </li><span>New Invoice<h2>40</h2>
              <div class="outline small invoces">
                <div class="inline smally invoice">
                </div>
              </div>
            </span>
            <li><img src="<?php echo base_url('assets/img/folder.svg') ?>"width="100">
            </li><span>Work Orders<h2>53</h2>
              <div class="outline small orders">
                <div class="inline smally order">
                </div>
              </div>
            </span>
            <li><img src="<?php echo base_url('assets/img/handshake.svg') ?>" alt="" width="100">
            </li><span>Members<h2>96</h2>
              <div class="outline small members">
                <div class="inline smally member">
                </div>
              </div>
            </span>
        </div>
        <div class="product">
          <div class="like">
            <span>Feedback</span><br>
            <img src="<?php echo base_url('assets/img/feedback.svg') ?>" width="55"><br>
            <span>2,033</span>
          </div>
          <div class="comment">
            <span>Comentar</span><br>
            <img src="<?php echo base_url('assets/img/comment.svg') ?>" width="55"><br>
            <span>23</span>
          </div>
        </div>
        <div class="product">
          <div class="comment">
            <span>Web Visitor</span><br>
            <img src="<?php echo base_url('assets/img/eye.svg') ?>" width="55"><br>
            <span>140.233/per Day</span>
          </div>
          <div class="comment view">
            <span>Total Visitor</span><br>
              <h1>135.787.456</h1>
          </div>
        </div>
        <div class="product">
          <div class="comment value">
            <h2>Total Revenue</h2>
            <img src="<?php echo base_url('assets/img/rp.png') ?>" width="100"><br>
            <span>Rp 5.300.234.143</span>
          </div>
        </div>
        <div class="graphics">
            <h1>Progress</h1><br>
            <span><b>Project Android   :</b></span><br>
            <div class="outline clients">
              <div class="inline client">
                10%
              </div>
            </div><br><br>
            <span><b>Project Ios   :</b></span>
            <div class="outline orders">
              <div class="inline order">
                46%
              </div>
            </div><br><br>
            <span><b>Project  SKP  :</b></span>
            <div class="outline members">
              <div class="inline member">
                97%
              </div>
            </div><br><br>
            <span><b>Project Antrian  :</b></span>
            <div class="outline">
              <div class="inline">
                80%
              </div>
            </div>
        </div>
    </div>
    <div class="profiles" id="profile">
      <div class="Photo">
        <img src="<?php echo base_url('assets/img/nickYong.png') ?>" width="200">
        <h3>Muhammad Nick Yong</h3><br>
        <h4>Web Developer</h4><br>
        <h5>CTO Cendana</h5>
        <span>Bio  :</span>
        <h6>
          Lorem ipsum dolor sit amet, consectetur
          adipisicing elit, sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua.
           Ut enim ad minim veniam, quis nostrud
           exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis
            aute irure dolor in reprehenderit in
             voluptate velit esse cillum dolore eu
             fugiat nulla pariatur. Excepteur sint
            occaecat cupidatat non proident, sunt in culpa
          qui officia deserunt mollit anim id est laborum.
        </h6><br><br>
        <span>Birth day: 20, Spetember 1995</span><br>
        <span>My Address: LuwokWaru , Malang Indonesia</span>
        <span>Work since : 12 September 2014</span>
        <button type="submit" name="button">Edit</button>
        <button type="button" name="button">Delete</button>
      </div>
        <div class="graphics skill">
            <h1>My Skills</h1><br>
            <span><b>System Embended   :</b></span><br>
            <div class="outline clients">
              <div class="inline client">
                10%
              </div>
            </div><br><br>
            <span><b>Backend   :</b></span>
            <div class="outline orders">
              <div class="inline order">
                46%
              </div>
            </div><br><br>
            <span><b>Front End  :</b></span>
            <div class="outline members">
              <div class="inline member">
                97%
              </div>
            </div><br><br>
            <span><b>Mobile APP  :</b></span>
            <div class="outline">
              <div class="inline">
                80%
              </div>
            </div>
        </div>
      </div>
    </div>
    <div class="Products" id="myProduct">
      <div class="add">
      <h1>My Product</h1><br><br>
        <div class="addProduct">
          <label>Add Product</label><br>
          <input type="text" name="" value=""><br>
          <label>Descrption   :</label>
          <textarea name="name" rows="8" cols="34"></textarea>
          <label>Foto</label>
          <input type="file" name="" class="inputfile">
          <button  name="butt">Upload</button>
        </div>
        <div class="edites">
          <span><b>Edit Products</b>
          </span>
          <button class="btn"type="submit" name="button">Edit</button><br>
          <button class="btn del" type="button" name="button">Delete</button>
          <button class="btn inf"type="submit" name="button">Edit</button><br>
          <button class="btn info" type="button" name="button">Delete</button>
          <button class="btn mov"type="submit" name="button">Edit</button><br>
          <button class="btn moving" type="button" name="button">Delete</button>
          <button class="btn vid"type="submit" name="button">Edit</button><br>
          <button class="btn video" type="button" name="button">Delete</button>

          <ul>
            <a href="<?=site_url('Antrian') ?>">
            <li>
              <h3>Digital Signage</h3>
              <img src="http://www.alatantrian.com/wp-content/uploads/2018/04/Digital-Signage-e1524724724317.jpg" width="250">
            </li>
            </a>
            <a href="<?=site_url('Antrian') ?>">
            <li>
              <h2>Anjungan Infomasi</h2>
              <img src="http://www.alatantrian.com/wp-content/uploads/2018/04/Anjungan-Informasi-e1524724703119.jpg" width="250">
            </li>
            </a>
            <a href="<?=site_url('Antrian') ?>">
            <li>
              <h3>Moving Sign</h3>
              <img src="http://www.alatantrian.com/wp-content/uploads/2018/04/Moving-Sign-e1524724684626.jpg" width="300">
            </li>
            </a>
            <a href="<?=site_url('Antrian') ?>">
            <li>
              <h3>Video Tron</h3>
              <img src="http://www.alatantrian.com/wp-content/uploads/2018/04/Video-Tron-e1524724693816.jpg" width="300">
              </li>
            </a>
          </ul>
        </div>
      </div>
    </div>
  </body>
</html>
